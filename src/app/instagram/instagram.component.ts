import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-instagram',
  templateUrl: './instagram.component.html',
  styleUrls: ['./instagram.component.css']
})
export class InstagramComponent implements OnInit {

  constructor(private auth: AuthenticationService) {
    console.log('Is authenticated: ' + this.auth.currentUserValue);
  }

  ngOnInit() {
  }

}
