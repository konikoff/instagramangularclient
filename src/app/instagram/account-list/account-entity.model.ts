export class AccountEntityModel {
  id: number;
  username: string;
  expiry: string;


  constructor(login: string, expiry: string) {
    this.username = login;
    this.expiry = expiry;
  }
}
