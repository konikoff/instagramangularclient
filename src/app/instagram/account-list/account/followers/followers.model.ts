import {FollowerModel} from './follower.model';

export class FollowersModel {
  followers: FollowerModel[];
  hasPrevious: boolean;
  hasNext: boolean;
  nextPageNumber: number;
  previousPageNumber: number;


  constructor(followers: FollowerModel[], hasPrevious: boolean, hasNext: boolean, nextPageNumber: number, previousPageNumber: number) {
    this.followers = followers;
    this.hasPrevious = hasPrevious;
    this.hasNext = hasNext;
    this.nextPageNumber = nextPageNumber;
    this.previousPageNumber = previousPageNumber;
  }
}
