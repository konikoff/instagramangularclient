export class FollowerModel {
  pictureUrl: string;
  username: string;
  isFavorite: boolean;
  instagramPk: number;


  constructor(pictureUrl: string, username: string, isFavorite: boolean, instagramPk: number) {
    this.pictureUrl = pictureUrl;
    this.username = username;
    this.isFavorite = isFavorite;
    this.instagramPk = instagramPk;
  }
}
