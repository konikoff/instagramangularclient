import {Component, HostListener, Input, OnInit} from '@angular/core';
import {InstagramService} from '../../../../services/instagram/instagram.service';
import {ActivatedRoute} from '@angular/router';
import {FollowersModel} from './followers.model';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {

  @Input() actionEnum;

  public followers: FollowersModel;

  private accountName: string;

  constructor(private route: ActivatedRoute,
              private instagramSrv: InstagramService) {
    this.route.params.subscribe(params => {
      console.log(params);
      this.accountName = params.username;
    });
  }

  ngOnInit() {
    this.instagramSrv.getFollowerUserList(this.accountName, this.actionEnum, 0)
      .subscribe((response: FollowersModel) => {
        this.followers = response;
        console.log('Followers');
        console.log(this.followers);
        console.log('Amount of followers: ' + this.followers.followers.length);
      });

  }

  onUnfollow(instagramPk: number) {
    this.instagramSrv.unfollowById(this.accountName, instagramPk)
      .subscribe((response) => {
        console.log('Unfollowed user' + instagramPk);
        console.log(response);
      });
  }

  @HostListener('window:scroll', [])
  onScrollBottom(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      // you're at the bottom of the page
      if (this.followers.hasNext) {
        console.log('Should be the bottom!');
        this.instagramSrv.getFollowerUserList(this.accountName, this.actionEnum, this.followers.nextPageNumber)
          .subscribe((response: FollowersModel) => {
              console.log('Retrieved new posts');
              console.log(response);

              for (const followerUser of response.followers) {
                this.followers.followers.push(followerUser);
              }
              this.followers.nextPageNumber = response.nextPageNumber;
              this.followers.hasNext = response.hasNext;
            }
          );
      }
    }
  }
}
