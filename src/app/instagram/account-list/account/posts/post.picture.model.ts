export class PostPictureModel {
  description: string;
  picUrl: string;
  likesCount: number;
  commentsCount: number;
  mediaId: number;

  constructor(description: string, picUrl: string, likesCount: number, commentsCount: number, mediaId: number) {
    this.description = description;
    this.picUrl = picUrl;
    this.likesCount = likesCount;
    this.commentsCount = commentsCount;
    this.mediaId = mediaId;
  }
}


