import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {InstagramService} from '../../../../services/instagram/instagram.service';
import {PostsModel} from './posts.model';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  public posts: PostsModel;
  private accountName: string;

  constructor(private route: ActivatedRoute,
              private instagramSrv: InstagramService) {
    this.route.params.subscribe(params => {
      console.log(params);
      this.accountName = params.username;
    });
  }

  ngOnInit() {
    this.instagramSrv.getPostsByInstagramUser(this.accountName, '').subscribe((response: PostsModel) => {
      console.log('Posts');
      this.posts = response;
      console.log(response);
    });
  }

  onShowLikes() {
    console.log('Show likes!');
  }

  onShowComments() {
    console.log('Show comments!');
  }

  @HostListener('window:scroll', [])
  onScrollBottom(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      // you're at the bottom of the page
      console.log('Should be the bottom!');
      this.instagramSrv.getPostsByInstagramUser(this.accountName, this.posts.nextMaxId).subscribe((response: PostsModel) => {
        console.log('Retrieved new posts');
        console.log(response);
        for (const postItem of response.posts) {
          this.posts.posts.push(postItem);
        }
        this.posts.nextMaxId = response.nextMaxId;
      });
    }
  }
}
