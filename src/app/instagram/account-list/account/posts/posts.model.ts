import { PostPictureModel } from './post.picture.model';

export class PostsModel {
  posts: PostPictureModel[];
  nextMaxId: string;


  constructor(posts: PostPictureModel[], nextMaxId: string) {
    this.posts = posts;
    this.nextMaxId = nextMaxId;
  }
}
