import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  public showNewTask = false;

  constructor() {
  }

  ngOnInit() {

  }

  onCreateNewTask() {
    this.showNewTask = !this.showNewTask;
  }

  onTaskCancelled(isCanceled: boolean) {
    this.showNewTask = isCanceled;
  }
}
