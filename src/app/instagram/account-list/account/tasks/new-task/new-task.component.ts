import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TasksComponent} from '../tasks.component';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit {

  @Output() taskCanceled = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  onSaveTask() {

  }

  onCancelTask() {
    this.taskCanceled.emit(false);
  }
}
