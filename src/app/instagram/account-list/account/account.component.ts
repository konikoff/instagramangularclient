import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ActionEnum} from '../../shared/action.enum';
import {InstagramService} from '../../../services/instagram/instagram.service';
import {AccountModel} from './account.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  private accountName: string;
  public actionEnum: typeof ActionEnum = ActionEnum;
  public action: ActionEnum = ActionEnum.default;
  public instagramAccount: AccountModel;

  constructor(private route: ActivatedRoute,
              private instagramSrv: InstagramService) {
    this.route.params.subscribe(params => {
      console.log(params);
      this.accountName = params.username;
    });
  }

  ngOnInit() {
    console.log('Get details for instgaram account: ' + this.accountName);
    this.instagramSrv.getDetailsByInstagramUser(this.accountName).subscribe((response: AccountModel) => {
      console.log('RetrievedDetails');
      console.log(response);
      this.instagramAccount = response;
    });
  }

  onSetAction(action: ActionEnum) {
    this.action = action;
  }
}
