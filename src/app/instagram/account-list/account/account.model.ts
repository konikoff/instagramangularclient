export class AccountModel {
  accountUrl: string;
  followerCount: number;
  followingCount: number;
  mutualCount: number;
  unmutualCount: number;
  fullName: string;
  imageUrl: string;
  posts: number;
  username: string;

  constructor(accountUrl: string,
              followerCount: number,
              followingCount: number,
              mutualCount: number,
              unmutualCount: number,
              fullName: string,
              imageUrl: string,
              posts: number,
              username: string) {
    this.accountUrl = accountUrl;
    this.followerCount = followerCount;
    this.followingCount = followingCount;
    this.mutualCount = mutualCount;
    this.unmutualCount = unmutualCount;
    this.fullName = fullName;
    this.imageUrl = imageUrl;
    this.posts = posts;
    this.username = username;
  }
}
