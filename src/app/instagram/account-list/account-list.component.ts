import {Component, OnInit} from '@angular/core';
import {AccountEntityModel} from './account-entity.model';
import {InstagramService} from '../../services/instagram/instagram.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {

  accountList: AccountEntityModel[];

  constructor(private instagramSrv: InstagramService) {
  }

  ngOnInit() {
    console.log('Account list init');
    this.updateAccounts();

  }

  onLogout(account: AccountEntityModel) {
    this.instagramSrv.logout(account.username).subscribe((response: AccountEntityModel[]) => {
      this.accountList = response;
    });
    console.log(account);
  }

  private updateAccounts() {
    this.instagramSrv.retrieveAllInstagramAccounts().subscribe((response: AccountEntityModel[]) => {
      this.accountList = response;
    });
  }
}
