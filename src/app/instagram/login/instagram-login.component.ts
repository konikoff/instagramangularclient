import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {InstagramLoginModel} from './instagram-login.model';
import {InstagramService} from '../../services/instagram/instagram.service';
import {AccountEntityModel} from '../account-list/account-entity.model';

@Component({
  selector: 'app-instagram-login',
  templateUrl: './instagram-login.component.html',
  styleUrls: ['./instagram-login.component.css']
})
export class InstagramLoginComponent implements OnInit {

  checkoutForm;

  constructor(private formBuilder: FormBuilder,
              private instagramService: InstagramService) {
    this.checkoutForm = this.formBuilder.group({
      login: '',
      password: ''
    });
  }

  ngOnInit() {
  }

  onSubmitInstagramLogin(loginData: InstagramLoginModel) {
    console.log('Instagram Login');
    console.log(loginData);
    this.instagramService.login(loginData).subscribe((response: AccountEntityModel) => {
      console.log(response);
    });
  }
}
