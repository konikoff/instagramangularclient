export enum ActionEnum {
  default,
  posts,
  followers,
  following,
  mutual,
  unmutual,
  tasks
}
