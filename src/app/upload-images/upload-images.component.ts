import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {InstagramService} from '../services/instagram/instagram.service';
import {ImageSnippet} from './image-snippet';


@Component({
  selector: 'app-upload-images',
  templateUrl: './upload-images.component.html',
  styleUrls: ['./upload-images.component.css']
})
export class UploadImagesComponent implements OnInit {

  fileName = 'Choose file';
  // tslint:disable-next-line:max-line-length
  imgURLTemplate = 'https://images.unsplash.com/photo-1421789665209-c9b2a435e3dc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjExMjU4fQ&dpr=2&auto=format&fit=crop&w=416&h=312&q=60';
  imgURL: string | ArrayBuffer = this.imgURLTemplate;
  selectedFile: ImageSnippet;

  constructor(private router: Router,
              private instagramSrv: InstagramService) {
  }

  ngOnInit() {
  }

  processImage(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = () => {
      this.selectedFile = new ImageSnippet(reader.result, file);

      this.imgURL = reader.result;
    };


    this.fileName = file.name;

  }

  private onSuccess() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'ok';
  }

  private onError() {
    this.selectedFile.pending = false;
    this.selectedFile.status = 'fail';
    this.selectedFile.src = '';
  }

  onSubmitImage(comment?: HTMLTextAreaElement) {
    if (comment.textLength < 0) {
      this.selectedFile.comment = comment.textContent;
    }

    if (this.selectedFile !== undefined) {

      this.instagramSrv.uploadImage('', this.selectedFile.file !== undefined ? this.selectedFile : null).subscribe(
        () => {
          this.onSuccess();
        },
        () => {
          this.onError();
        }
      );
    } else {
      throw new Error('The image wasn\'t chosen!');
    }
  }

  onCancel() {
    this.imgURL = this.imgURLTemplate;
  }
}
