export class ImageSnippet {
  pending = false;
  status = 'init';
  comment = '';

  constructor(public src: string | ArrayBuffer,
              public file: File) {
  }

}
