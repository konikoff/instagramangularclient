import {Component, OnInit} from '@angular/core';
import {RegistrationFormModel} from './registration-form.model';
import {AuthenticationService} from '../../services/authentication.service';
import {StatusModel} from '../../services/status.model';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  public registrationForm: RegistrationFormModel = new RegistrationFormModel('', '', '', '');

  constructor(private authService: AuthenticationService) {
  }

  ngOnInit() {
  }

  onRegister() {
    console.log('Registration Form');
    console.log(this.registrationForm);
    this.authService.registerNewUser(this.registrationForm).subscribe((response: StatusModel) => {
      console.log('Status: ' + response.status);
    });
  }
}
