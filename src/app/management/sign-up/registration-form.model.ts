export class RegistrationFormModel {
  fullName: string;
  email: string;
  password: string;
  confirmPassword: string;

  constructor(fullName: string, email: string, password: string, confirmPassword: string) {
    this.fullName = fullName;
    this.email = email;
    this.password = password;
    this.confirmPassword = confirmPassword;
  }
}
