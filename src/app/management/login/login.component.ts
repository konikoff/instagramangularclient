import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials = {username: '', password: ''};

  constructor(private auth: AuthenticationService,
              private http: HttpClient,
              private router: Router) {
  }

  ngOnInit() {
  }

  onLogin() {
    this.auth.authenticate(this.credentials, () => {
      this.router.navigateByUrl( '/').then(r => {
        console.log('Something went wrong');
      });
    });
  }

}
