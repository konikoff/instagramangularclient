import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {InstagramComponent} from './instagram/instagram.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {InstagramLoginComponent} from './instagram/login/instagram-login.component';
import {AccountListComponent} from './instagram/account-list/account-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccountComponent} from './instagram/account-list/account/account.component';
import {PostsComponent} from './instagram/account-list/account/posts/posts.component';
import {FollowersComponent} from './instagram/account-list/account/followers/followers.component';
import {TasksComponent} from './instagram/account-list/account/tasks/tasks.component';
import {ManagementComponent} from './management/management.component';
import {LoginComponent} from './management/login/login.component';
import {SignUpComponent} from './management/sign-up/sign-up.component';
import {AuthenticationService} from './services/authentication.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {XsrfInterceptor} from './security/xhr-interceptor.module';
import {InstagramService} from './services/instagram/instagram.service';
import {AuthGuardService} from './services/auth-guard.service';
import {ErrorInterceptor} from './interceptors/error.interceptor';
import {JwtInterceptor} from './interceptors/jwt.interceptor';
import {NewTaskComponent} from './instagram/account-list/account/tasks/new-task/new-task.component';
import {UploadImagesComponent} from './upload-images/upload-images.component';


const appRoutes: Routes = [
  {path: 'instagram', component: InstagramComponent, canActivate: [AuthGuardService]},
  {path: 'instagram/:username', component: AccountComponent, canActivate: [AuthGuardService]},
  {path: 'auth/sign-in', component: LoginComponent},
  {path: 'auth/sign-up', component: SignUpComponent},
  {path: 'upload_images', component: UploadImagesComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    InstagramComponent,
    PageNotFoundComponent,
    InstagramLoginComponent,
    AccountListComponent,
    AccountComponent,
    PostsComponent,
    FollowersComponent,
    TasksComponent,
    ManagementComponent,
    LoginComponent,
    SignUpComponent,
    NewTaskComponent,
    UploadImagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}
    ),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthenticationService, InstagramService,
    {provide: HTTP_INTERCEPTORS, multi: true, useClass: XsrfInterceptor},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {

}
