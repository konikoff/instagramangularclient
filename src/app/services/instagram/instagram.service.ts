import {Injectable} from '@angular/core';
import {Constants} from '../constants';
import {HttpClient, HttpParams} from '@angular/common/http';

import {ActionEnum} from '../../instagram/shared/action.enum';
import {InstagramLoginModel} from '../../instagram/login/instagram-login.model';
import {ImageSnippet} from '../../upload-images/image-snippet';

@Injectable()
export class InstagramService {
  static readonly INSTAGRAM_API = '/api/instagram';

  constructor(private http: HttpClient) {
  }

  retrieveAllInstagramAccounts() {
    return this.http.get(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/get-all');
  }

  login(instagramLogin: InstagramLoginModel) {
    return this.http.post(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/login', instagramLogin);
  }

  logout(instagramUser: string) {
    return this.http.delete(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/logout/' + instagramUser);
  }

  getDetailsByInstagramUser(instagramUser: string) {
    return this.http.get(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/' + instagramUser);
  }

  getPostsByInstagramUser(instagramUser: string, nextMaxId: string) {
    let params = new HttpParams();

    if (nextMaxId !== undefined) {
      params = params.append('next_max_id', nextMaxId);
    }

    return this.http.get(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/' + instagramUser + '/posts', {params});
  }

  getFollowerUserList(instagramUser: string, action: ActionEnum, nextPageNumber: number) {
    let params = new HttpParams();

    if (nextPageNumber !== undefined) {
      params = params.append('next_page_number', String(nextPageNumber));
    }

    let followerType: string;
    switch (action) {
      case ActionEnum.followers: {
        console.log(action);
        followerType = 'followers';
        break;
      }
      case ActionEnum.following: {
        console.log(action);
        followerType = 'following';
        break;
      }
      case ActionEnum.mutual: {
        console.log(action);
        followerType = 'mutual';
        break;
      }
      case ActionEnum.unmutual: {
        console.log(action);
        followerType = 'unmutual';
        break;
      }
      default: {
        console.log(action);
        break;
      }
    }
    return this.http.get(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/' + instagramUser + '/' + followerType, {params});
  }

  unfollowById(instagramUser: string, unfollowId: number) {
    let params = new HttpParams();

    if (unfollowId !== undefined) {
      params = params.append('unfollow_id', unfollowId.toString());
    }
    return this.http.delete(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/' + instagramUser + '/unfollow', {params});
  }

  uploadImage(instagramUser: string, imageSnippet: ImageSnippet) {
    if (imageSnippet.file !== null) {
      const formData = new FormData();

      formData.append('image', imageSnippet.file);
      formData.append('comment', imageSnippet.comment);

      return this.http.post(Constants.BACKEND_URL + InstagramService.INSTAGRAM_API + '/' + instagramUser + '/upload-image', formData);
    } else {
      throw  new Error('Image is not set, try again!');
    }

  }
}

