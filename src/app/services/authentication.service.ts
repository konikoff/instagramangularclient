import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserModel} from './user.model';
import {RegistrationFormModel} from '../management/sign-up/registration-form.model';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
  readonly URL = 'http://localhost:8080';

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;


  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(localStorage.getItem('currentUser'));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  authenticate(credentials, callback) {
    const headers = new HttpHeaders(credentials ? {
        authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)
      } : {}
    );

    console.log(headers);

    this.http.get(this.URL + '/api/user', {headers}).subscribe((response: UserModel) => {
        console.log('Auth response:');
        console.log(response);

        if (response.name) {
          localStorage.setItem('currentUser', response.name);
          this.currentUserSubject.next(response.name);
        } else {
        }
        return callback && callback();
      },
      error => {
        console.log(error);
      });
  }

  login(username, password) {
    return this.http.post<any>(this.URL + '/api/user', {username, password})
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  registerNewUser(registrationForm: RegistrationFormModel) {
    return this.http.post(this.URL + '/api/sign-up', registrationForm);
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

}
